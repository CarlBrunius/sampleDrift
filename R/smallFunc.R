#' Root mean squared distance
#'
#'Calculate root mean squared distance from center point
#' @param mat a matrix containing observations as rows and variables as columns
#' @return a numeric rmsDist
#' @export
## Simple function for calculating root mean square distance
rmsDist=function(mat) {
  mean=colMeans(mat)
  rmsd=sqrt(sum(apply(mat,1,function(x) sum((x-mean)^2)))/nrow(mat))
  return(rmsd)
}

#' Coefficient of variation (CV)
#'
#' Calculates CV per column in a matrix. Aka relative standard deviation (RSD).
#' @param mat a matrix with variables as columns
#' @return a vector of CVs
#' @export
## Simple function for calculating cv per column (ie variable)
cv=function(mat) {
  if (is.null(dim(mat))) {
    cv=sd(mat)/mean(mat)
  } else {
    mean=apply(mat,2,mean)
    sd=apply(mat,2,sd)
    cv=sd/mean
  }
  return(cv)
}
