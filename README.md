# Model and predict effects of pre-analytical sample management on OMICs data 

## Description
This is a package dedicated to improving metabolomics and other OMICs data by taking into consideration the effects of pre-analytical sample management on the analyte profiles. The information gained is then used to reduce those errors by back-calculating the modelled error functions from pre-analytical sample management. The package and associated workflow has core functions for: 
- Modelling of effects of pre-analytical sample management on analyte profile
- Finding markers for pre-analytical sample management.
- Prediction of "accurate" analyte profile using either available meta-data OR markers obtained from sample analyte profile.

Prediction using available metadata is useful in the case of cohorts in which this information this is stored and accessible. Predictions using markers represents a less accurate approach, but nonetheless can represent an increase in sample and information quality in the case of legacy samples or other samples for which pre-analytical sample management metadata isn't available.

## Installation
Install `devtools` to be able to install packages from GitHub.

Install `sampleDrift` package by:

`devtools::install_git("https://gitlab.com/CarlBrunius/sampleDrift.git")`

In addition to functions relevant for pre-analytical sample management modelling and prediction, data is provided to accurately reproduce figures from the original *Brunius et al* paper (see below).

## Workflow
After installation, a `Workflow_Example` folder is created in the `sampleDrift` library (in your R library folder). Within this folder, there is a `workflow.R` script containing code on how the package was used to perform pre-analytical sample management modelling and prediction and reproduce figures from the original *Brunius et al* paper (see below).

## Reference
The development and inner workings of these algorithms are reported in:

*Brunius C et al, 2017. Manuscript.*

## Version history
version | date  | comment
:-----: | :---: | :------
0.1.001 | 2017-03-22 | PQN data, cleaned up workflow, NMR functions to separate `NMRtools` package
0.0.930 | 2016-05-23 | `statNMR` In-house flavour of `STOCSY`
0.0.920 | 2016-05-12 | `sensPlot`, `biplotPLS`, workflow for *All data* -> All multivariate modelling & permutations.
0.0.910 | 2016-03-31 | Working build for entire workflow *140Data* / 22C.
0.0.900 | 2016-03-22 | First commit. Clustering and drift functions. Workflow for biomarker identification/predictive modelling.
